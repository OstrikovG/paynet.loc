<?php

namespace OstrikovG\Tests\Unit;

use OstrikovG\Resource\Info;
use PHPUnit\Framework\TestCase;

class InfoTest extends TestCase
{
    public function testGetSaleInfoResponseStructure()
    {
        $info = new Info();
        $response = $info->getSaleInfo();
        $data = json_decode($response, true);
        $this->assertArrayHasKey('agent', $data);
        $this->assertArrayHasKey('pos', $data);
        $this->assertArrayHasKey('support', $data);
    }
}