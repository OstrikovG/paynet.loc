<?php

namespace OstrikovG\Tests\Unit;

use OstrikovG\Resource\Balance;
use PHPUnit\Framework\TestCase;

class BalanceTest extends TestCase
{
    public function testGetActiveWalletsResponseStructure()
    {
        $balance = new Balance();
        $response = $balance->getActiveWallets();
        $data = json_decode($response, true);

        $this->assertArrayHasKey('currency', $data);
//        $this->assertArrayHasKey('wallets', $data);
    }
}