<?php

namespace OstrikovG\Tests\Unit;

use OstrikovG\Resource\Service;
use PHPUnit\Framework\TestCase;

class ServiceTest extends TestCase
{
    public function testGetServicesResponseStructure()
    {
        $service = new Service();
        $response = $service->getServices();
        $data = json_decode($response, true);
        $this->assertArrayHasKey('services', $data);
    }
}