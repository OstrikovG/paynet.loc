<?php

namespace OstrikovG\Tests\Unit;

use OstrikovG\Resource\Transaction;
use PHPUnit\Framework\TestCase;

class TransactionTest extends TestCase
{
    public function testCreateTransactionResponseStructure()
    {
        $arrayData = [
            "simulation" => true,
            "product" => "du-mt",
            "fields" => [
                "customer" => "971550000000",
                "account" => "971550000000",
                "amount" => 5.00,
            ]
        ];
        $transaction = new Transaction();
        $response = $transaction->createTransaction($arrayData);
        $data = json_decode($response, true);
        $this->assertArrayHasKey('transaction', $data);

        return $data['transaction']['id']; // transactionId

    }

    /**
     * @depends testCreateTransactionResponseStructure
     */
    public function testConfirmTransactionResponseStructure($transactionId)
    {
        $arrayData = [
            "simulation" => true,
            "transaction_id" => $transactionId
        ];
        $transaction = new Transaction();
        $response = $transaction->confirmTransaction($arrayData);
        $data = json_decode($response, true);
        $this->assertArrayHasKey('transaction', $data);
    }

    /**
     * @depends testCreateTransactionResponseStructure
     */
    public function testFindTransactionResponseStructure($transactionId)
    {
        $arrayData = [
            "simulation" => true,
            "transaction_id" => $transactionId
        ];
        $transaction = new Transaction();
        $response = $transaction->findTransaction($arrayData);
        $data = json_decode($response, true);
        $this->assertArrayHasKey('transaction', $data);
    }
}