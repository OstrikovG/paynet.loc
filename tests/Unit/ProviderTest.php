<?php

namespace OstrikovG\Tests\Unit;

use OstrikovG\Resource\Provider;
use PHPUnit\Framework\TestCase;

class ProviderTest extends TestCase
{
    public function testGetCountriesResponseStructure()
    {
        $provider = new Provider();
        $response = $provider->getCountries();
        $data = json_decode($response, true);

        $this->assertArrayHasKey('countries', $data);
    }

    public function testGetAvailableProvidersResponseStructure()
    {
        $arrayData = [
            "country_id" => 17
        ];
        $provider = new Provider();
        $response = $provider->getAvailableProviders($arrayData);
        $data = json_decode($response, true);

        $this->assertArrayHasKey('country', $data);
        $this->assertArrayHasKey('providers', $data);
    }

    public function testGetAvailableOperatorsPricesResponseStructure()
    {
        $arrayData = [
            "provider_id" => 46
        ];
        $provider = new Provider();
        $response = $provider->getAvailableOperatorsPrices($arrayData);
        $data = json_decode($response, true);

        $this->assertArrayHasKey('country', $data);
        $this->assertArrayHasKey('provider', $data);
        $this->assertArrayHasKey('amounts', $data);
    }
}