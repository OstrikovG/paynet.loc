###### How to start the project

1. `composer install`
2. `cp env.example.php env.php`

###### Api Queries To Gulfbox From Postman
https://www.getpostman.com/collections/a3b13a5fc037a3319983 

###### How to run unit tests
`composer test-unit`