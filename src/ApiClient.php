<?php

namespace OstrikovG;

class ApiClient
{
    const API_URL = 'http://api.gulfbox.ae';

    const SYSTEM_ERRORS = [
        1 => 'Auth Failed',
        2 => 'Bad request',
        20 => 'Transaction not found',
        22 => 'External transaction not found',
        23 => 'Could not get transaction info',
        50 => 'Wrong input data',
        60 => 'Service provider error',
        101 => 'Product is not available',
        105 => 'External transaction id already exists',
        1000 => 'You have exceeded your transactions limit',
        1001 => 'Not enough funds for this transaction',
        1200 => 'This customer is banned in our system',
        201 => 'Wrong account number'
    ];

    public function run($serviceUrl, $arrayData = [])
    {
        $postData = self::getJsonData($arrayData);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $serviceUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postData,
            CURLOPT_HTTPHEADER => array('Content-Type:application/json'),
        ));
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }


    private  static function getJsonData($arrayData)
    {
        $authArray = self::getArrayAuthSection();
        $data = json_encode(array_merge($authArray, $arrayData));
        return $data;
    }

    private static function getArrayAuthSection(): array
    {
        $timestamp = time()*1000;

        $id = env('GULFBOX_CLIENT_ID');
        $key = $timestamp;
        $hash = self::getHash($timestamp);

        $authArray = [
            "auth" => [
                "id" => $id,
                "key" => $key,
                "hash" => $hash
            ]
        ];
        return $authArray;

    }

    private static function getHash($timestamp)
    {
        $key = $timestamp;
        $id = env('GULFBOX_CLIENT_ID');
        $pointToken = env('GULFBOX_CLIENT_TOKEN');
        $hash = md5($id.$pointToken.$key);
        return $hash;
    }
}