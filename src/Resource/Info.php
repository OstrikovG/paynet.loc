<?php

namespace OstrikovG\Resource;

class Info extends AbstractResource
{
    public function getSaleInfo()
    {
        $apiClient = $this->apiClient;
        $serviceUrl = $apiClient::API_URL . '/' . 'info';

        $response = $apiClient->run($serviceUrl);
        return $response;
    }
}