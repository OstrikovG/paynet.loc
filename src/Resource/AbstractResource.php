<?php

namespace OstrikovG\Resource;

use OstrikovG\ApiClient;

abstract class AbstractResource
{
    /* @var ApiClient */
    public $apiClient;

    public function __construct()
    {
        $this->apiClient = new ApiClient();
    }
}