<?php

namespace OstrikovG\Resource;

class Transaction extends AbstractResource
{
    const TRANSACTION_STATES = [
        -1 => 'New transaction, not confirmed',
        0 => 'Pending',
        1 => 'Successful',
        2 => 'Failed',
        3 => 'Cancelled',
        4 => 'Replaced, manual action, only by request'
    ];

    public function createTransaction(array $arrayData)
    {
//        $arrayData = [
//            "product" => "du-mt",
//            "fields" => [
//                "customer" => "971550000000",
//                "account" => "971550000000",
//                "amount" => 5.00,
//            ]
//        ];
        $apiClient = $this->apiClient;
        $serviceUrl = $apiClient::API_URL . '/' . 'transaction/check';

        $response = $apiClient->run($serviceUrl, $arrayData);
        return $response;
    }

    public function confirmTransaction(array $arrayData)
    {
//        $arrayData = [
//            "transaction_id" => "123-123-123-123"
//        ];
        $apiClient = $this->apiClient;
        $serviceUrl = $apiClient::API_URL . '/' . 'transaction/confirm';

        $response = $apiClient->run($serviceUrl, $arrayData);
        return $response;
    }

    public function findTransaction(array $arrayData)
    {
//        $arrayData = [
//            "transaction_id" => "123-123-123-123"
//        ];
        $apiClient = $this->apiClient;
        $serviceUrl = $apiClient::API_URL . '/' . 'transaction/find';

        $response = $apiClient->run($serviceUrl, $arrayData);
        return $response;
    }
}