<?php

namespace OstrikovG\Resource;

class Balance extends AbstractResource
{
    public function getActiveWallets()
    {
        $apiClient = $this->apiClient;
        $serviceUrl = $apiClient::API_URL . '/' . 'balance';

        $response = $apiClient->run($serviceUrl);
        return $response;
    }
}