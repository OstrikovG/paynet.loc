<?php

namespace OstrikovG\Resource;

class Service extends AbstractResource
{
    public function getServices()
    {
        $apiClient = $this->apiClient;
        $serviceUrl = $apiClient::API_URL . '/' . 'services';

        $response = $apiClient->run($serviceUrl);
        return $response;
    }

}