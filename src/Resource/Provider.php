<?php

namespace OstrikovG\Resource;

class Provider extends AbstractResource
{
    public function getCountries()
    {
        $apiClient = $this->apiClient;
        $serviceUrl = $apiClient::API_URL . '/' . 'providers';

        $response = $apiClient->run($serviceUrl);
        return $response;
    }

    public function getAvailableProviders(array $arrayData)
    {
//        $arrayData = [
//            "country_id" => 17
//        ];
        $apiClient = $this->apiClient;
        $serviceUrl = $apiClient::API_URL . '/' . 'providers';

        $response = $apiClient->run($serviceUrl, $arrayData);
        return $response;
    }


    public function getAvailableOperatorsPrices(array $arrayData)
    {
//        $arrayData = [
//            "provider_id" => 46
//        ];
        $apiClient = $this->apiClient;
        $serviceUrl = $apiClient::API_URL . '/' . 'providers';

        $response = $apiClient->run($serviceUrl, $arrayData);
        return $response;
    }
}